import random
from random import randint


name = input("Hi! What is your name? ")

for guess_number in range(1, 6):
    month_number = randint(1,12)
    year_number = randint(1924, 2010)

    print("Guess", guess_number, ",", "were you born in",
          month_number, "/", year_number, "?")

    response = input("Yes or no? " )

    if response == "yes" or "Yes":
        print("I knew it!")
        exit()
    elif guess_number == 5:
        print("I have better things to do. Goodbye.")
    else:
        print("Drat! Lemme try again!")
        

    
    